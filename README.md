[Okta's Identity Engine]: https://developer.okta.com/docs/concepts/ie-intro/
[Okta Auth JS]: https://github.com/okta/okta-auth-js

# Embedded Auth with SDKs Sample Application

## Introduction

> :grey_exclamation: The use of this Sample uses an SDK that requires usage of the Okta Identity Engine. This functionality is in general availability but is being gradually rolled out to customers. If you want
to request to gain access to the Okta Identity Engine, please reach out to your account manager. If you do not have an account manager, please reach out to oie@okta.com for more information.

This Sample Application will show you the best practices for integrating Authentication into your app
using [Okta's Identity Engine][]. Specifically, this application will cover some basic needed use cases to get you up and running quickly with Okta.
These Examples are:

1. Sign In
2. Sign Out
5. Sign In with Multifactor Authentication using Email or Phone


## Installation & Running The App

By default the app server runs at `http://localhost:3000`.

These values must exist as environment variables. They can be exported in the shell, or saved in a file named `.env`, located in the root level of the sample project. See [dotenv](https://www.npmjs.com/package/dotenv) for more details on this file format.

```ini
ISSUER=https://REPLACE_THIS/oauth2/default
CLIENT_ID=123...REPLACE_THIS...123
CLIENT_SECRET=456...REPLACE_THIS
APP_BASE_URL=http://localhost
NODE_TLS_REJECT_UNAUTHORIZED=0
```

**NODE_TLS_REJECT_UNAUTHORIZED** would only be required for local environment testing.

### Commands

| Command               | Description                    |
| --------------------- | ------------------------------ |
| `yarn install`        | Install the dependencies |
| `yarn start`          | Starts the app server |

